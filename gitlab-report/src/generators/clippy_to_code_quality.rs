// MIT License
//
// Copyright (c) 2021-2023 Tobias Pfeiffer
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use super::*;

pub fn run(
	reader: impl io::BufRead,
	mut writer: impl io::Write
) {
	let mut issues = Vec::new();

	for line in reader.lines() {
		let msg = match line.and_then(|line| serde_json::from_str(&line)
			.map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e)))
		{
			Ok(v) => v,
			Err(e) => {
				eprintln!("error: failed to parse message: {e}");
				std::process::exit(1);
			}
		};

		let msg = match msg {
			clippy::Message::CompilerMessage(v) if !v.message.spans.is_empty() => v,
			_ => continue
		};

		let mut hasher = xxhash_rust::xxh3::Xxh3::new();
		std::hash::Hash::hash(&msg.message, &mut hasher);
		let fingerprint = hasher.digest128();

		issues.push(code_climate::CodeQualityReportIssue {
			r#type:             code_climate::CODE_QUALITY_REPORT_TYPE,
			check_name:         msg.message.code.as_ref()
				.map_or_else(|| "unknown".to_string(), |v| v.code.clone()),
			description:        msg.message.message.clone(),
			content:            Some(format!("```{}```", msg.message.rendered)),
			categories:         vec![code_climate::CodeQualityReportIssueCategory::Style],
			location:           msg.message.spans[0].clone().into(),
			other_locations:    (msg.message.spans.len() > 1).then(|| msg.message.spans[1..]
				.iter()
				.map(|span| span.clone().into())
				.collect()),
			remediation_points: None,
			severity:           Some(match &*msg.message.level {
				"error"   => code_climate::CodeQualityReportIssueSeverity::Major,
				"warning" => code_climate::CodeQualityReportIssueSeverity::Minor,
				_         => code_climate::CodeQualityReportIssueSeverity::Info
			}),
			fingerprint:        Some(format!("{fingerprint:x}"))
		});
	}

	eprintln!("  \x1b[32;1mGenerating\x1b[0m code quality report");

	if let Err(e) = serde_json::to_writer(&mut writer, &issues) {
		eprintln!("error: failed to generate report: {e}",);
		std::process::exit(1);
	}
}