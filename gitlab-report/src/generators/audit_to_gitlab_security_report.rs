// MIT License
//
// Copyright (c) 2021-2023 Tobias Pfeiffer
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use super::*;

pub fn run(
	format: gitlab_security_report::ScanType,
	reader: impl io::BufRead,
	writer: impl io::Write
) {
	let mut report = gitlab_security_report::Report {
		version:        "15.0.6".to_string(),
		scan:           gitlab_security_report::Scan {
			start_time: get_time(),
			end_time:   "".to_string(),
			r#type:     format,
			status:     gitlab_security_report::ScanStatus::Success,
			messages:   Vec::new(),
			analyzer:   gitlab_security_report::ScanAnalyzer {
				id:      "gitlab-report".to_string(),
				name:    "gitlab-report".to_string(),
				url:     Some("https://crates.io/crates/gitlab-report".to_string()),
				vendor:  gitlab_security_report::ScanVendor {
					name: "Tobias Pfeiffer".to_string()
				},
				version: env!("CARGO_PKG_VERSION").to_string(),
			},
			scanner:    gitlab_security_report::ScanScanner {
				id:      "cargo-audit".to_string(),
				name:    "Audit".to_string(),
				url:     Some("https://crates.io/crates/cargo-audit".to_string()),
				vendor:  gitlab_security_report::ScanVendor {
					name: "Rust Secure Code Working Group".to_string()
				},
				version: "?".to_string(),
			},
		},
		vulnerabilities:  Vec::new(),
		remediations:     Vec::new(),
		dependency_files: Vec::new()
	};
	let scanner    = gitlab_security_report::VulnerabilityScanner { id: "cargo_audit".to_string(), name: "Cargo Audit".to_string() };
	let audit      = match serde_json::from_reader::<_, audit::Report>(reader) {
		Ok(v) => v,
		Err(e) => {
			eprintln!("error: failed to parse report: {e}");
			std::process::exit(1);
		}
	};

	eprintln!("  \x1b[32;1mGenerating\x1b[0m SAST report");

	for vulnerability in audit.vulnerabilities.list {
		report.vulnerabilities.push(gitlab_security_report::Vulnerability {
			scanner: scanner.clone(),
			..audit_issue_to_gitlab_vuln(vulnerability, format)
		});
	}

	for (_, warnings) in audit.warnings {
		for warning in warnings {
			report.vulnerabilities.push(gitlab_security_report::Vulnerability {
				scanner: scanner.clone(),
				..audit_issue_to_gitlab_vuln(warning, format)
			});
		}
	}

	report.scan.end_time = get_time();

	if let Err(e) = serde_json::to_writer(writer, &report) {
		eprintln!("error: failed to generate report: {e}");
	}
}

fn audit_issue_to_gitlab_vuln(issue: audit::Issue, ty: gitlab_security_report::ScanType) -> gitlab_security_report::Vulnerability {
	gitlab_security_report::Vulnerability {
		category:    "Dependency Scanning".to_string(),
		severity:    Some(match issue.kind.as_deref() {
			None           => gitlab_security_report::VulnerabilitySeverity::High,
			Some("notice") => gitlab_security_report::VulnerabilitySeverity::Info,
			Some(_)        => gitlab_security_report::VulnerabilitySeverity::Medium
		}),
		name:        issue.advisory.as_ref().map(|v| v.id.clone())
			.or_else(|| issue.package.as_ref().map(|v| format!("{}@{}", v.name, v.version))),
		message:     issue.advisory.as_ref().map(|v| v.title.clone())
			.or_else(|| issue.package.as_ref()
				.map(|v| format!("{}@{} ({})", v.name, v.version, issue.kind.as_deref().unwrap_or("error")))),
		description: issue.advisory.as_ref().map(|v| v.description.clone()),
		confidence:  Some(gitlab_security_report::VulnerabilityConfidence::Confirmed),
		identifiers: issue.advisory.as_ref()
			.map(|v| gitlab_security_report::VulnerabilityIdentifier {
				r#type: "RUSTSEC Advisory".to_string(),
				name:   v.id.clone(),
				value:  v.id.clone(),
				url:    v.url.clone()
			})
			.into_iter()
			.collect(),
		location:   match ty {
			gitlab_security_report::ScanType::DependencyScanning => gitlab_security_report::VulnerabilityLocation::DependencyScanning {
				file:       None,
				dependency: gitlab_security_report::VulnerabilityLocationDependency {
					package:         issue.package.as_ref().map(|v| gitlab_security_report::VulnerabilityLocationDependencyPackage { name: v.name.clone() }),
					version:         issue.package.as_ref().map(|v| v.version.clone()),
					iid:             None,
					direct:          None,
					dependency_path: Vec::new()
				}
			},
			gitlab_security_report::ScanType::Sast => gitlab_security_report::VulnerabilityLocation::Sast {
				file:       None,
				start_line: None,
				end_line:   None,
				module:     None,
				item:       None
			},
			_ => unreachable!()
		},
		..Default::default()
	}
}